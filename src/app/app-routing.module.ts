import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WelcomePageComponent } from './pages/welcome-page/welcome-page.component';
import { ProductsPageComponent } from './pages/products-page/products-page.component';
import { ShowProductComponent } from './pages/show-product-page/show-product.component';
import { ProductComponent } from './components/product/product.component';



const routes: Routes = [
{
  path: '', component: WelcomePageComponent,
},
{
  path: 'products', component: ProductsPageComponent,
},
{
  path: 'products/:idProduct', component: ProductComponent,
},
{
  path: 'management', component: ShowProductComponent,
},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
