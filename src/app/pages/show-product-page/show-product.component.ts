import { ProductsLocalService } from './../../services/local/products-local.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-show-product',
  templateUrl: './show-product.component.html',
  styleUrls: ['./show-product.component.scss']
})
export class ShowProductComponent implements OnInit {

  public formGroupManagement;

  constructor(private formBuilder: FormBuilder, private productsLocalService: ProductsLocalService) { }

  ngOnInit(): void {

    this.formGroupManagement = this.formBuilder.group({
      name: ['Product', [Validators.required]],
      price: ['', [Validators.required]],
      description: ['Description', [Validators.required]],
      image: ['https://photos6.spartoo.es/photos/151/15199406/15199406_1200_A.jpg'],

    });

  }

  // tslint:disable-next-line: typedef
  addProduct() {
    this.productsLocalService.addProducts([this.formGroupManagement.value]);

  }
}


