import { ProductsLocalService } from './../../../../services/local/products-local.service';
import { ProductListService } from './../../../../services/product-list.service';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-products-gallery',
  templateUrl: './products-gallery.component.html',
  styleUrls: ['./products-gallery.component.scss']
})
export class ProductsGalleryComponent implements OnInit {

@Input () products = [];

  constructor(private productListService: ProductListService, private productsLocalService: ProductsLocalService) { }

  ngOnInit(): void {
  }

}
