import { ProductsLocalService } from './../../services/local/products-local.service';
import { Component, OnInit, Input } from '@angular/core';
import { ProductListService } from './../../services/product-list.service';

@Component({
  selector: 'app-products-page',
  templateUrl: './products-page.component.html',
  styleUrls: ['./products-page.component.scss']
})
export class ProductsPageComponent implements OnInit {

products = [];
  constructor(private productListService: ProductListService, private productsLocalService: ProductsLocalService) { }

  ngOnInit(): void {
    const productsLocalService = this.productsLocalService.getDataSimple();
    if (!productsLocalService.length){
    this.productListService.getDataSimple().subscribe( (products: any) => {
      this.products = products;
      this.productsLocalService.addProducts(products);
    })
  }else{
    this.products = productsLocalService;
  }
  }

  // tslint:disable-next-line: typedef
  filterProduct($event){
    this.productsLocalService.getProductFiltered($event.name).subscribe((res: any) => {
      this.products = res;
    });
  }
}
