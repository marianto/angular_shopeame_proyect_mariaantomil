import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { UrlHandlingStrategy } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class ProductListService {

constructor(private httpClient: HttpClient) { }
// tslint:disable-next-line: typedef
getDataSimple(){
return this.httpClient.get( environment.url);
}

// tslint:disable-next-line: typedef

}
