import { environment } from './../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProductsLocalService {

  products = [];

  constructor(private httpClient: HttpClient) { }

  getDataSimple(){
    return this.products;
  }
  // tslint:disable-next-line: typedef
  getProduct(idProduct){
    return this.httpClient.get( environment.url + idProduct);
    }
  // tslint:disable-next-line: typedef
  getProductFiltered(filter){
      return this.httpClient.get( environment.url + 'product/?name' + filter);
    }

    // tslint:disable-next-line: typedef
    addProducts(products) {
      this.products = [...this.products, ...products]
    }
}
