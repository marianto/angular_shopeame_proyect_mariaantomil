import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

formSearchProduct;
@Output() filterEmitter = new EventEmitter()
  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.formSearchProduct = this.formBuilder.group({
      name: ['Vintage'],
      price: [''],
      description: [''],
    });
  }
  // tslint:disable-next-line: typedef
  filter() {
    this.filterEmitter.emit();
    this.filterEmitter.emit([this.formSearchProduct.get('name').value]);
  }
}

