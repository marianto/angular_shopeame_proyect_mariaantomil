import { ProductsLocalService } from './../../services/local/products-local.service';
import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

 product: any;

  constructor(private route: ActivatedRoute, private productsLocalService: ProductsLocalService) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      const idProduct = params.get('idProduct');
      this.productsLocalService.getProduct(idProduct).subscribe(product => {
        this.product = product;
  });

});

}

}
